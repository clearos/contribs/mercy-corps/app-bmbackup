<?php

/**
 * Baremetal Backup And Restore controller.
 *
 * @category   Apps
 * @package    baremetalbackup
 * @subpackage views
 * @author     Mahmood Khan <mkhan@mercycorps.org>
 * @copyright  2014 Mercy Corps
 * @license    http://www.gnu.org/copyleft/gpl.html GNU General Public License version 3 or later
 */

///////////////////////////////////////////////////////////////////////////////
// Load dependencies
///////////////////////////////////////////////////////////////////////////////

$this->lang->load('base');
$this->lang->load('bmbackup');

///////////////////////////////////////////////////////////////////////////////
// Form Handler 
///////////////////////////////////////////////////////////////////////////////
$buttons = array(
    form_submit_custom('update_schedule', lang('bmbackup_update_hour'))
);

$times = array();
$times[24] = 'Disabled';
$times[0] = '00:00';
$times[1] = '01:00';
$times[2] = '02:00';
$times[3] = '03:00';
$times[4] = '04:00';
$times[5] = '05:00';
$times[6] = '06:00';
$times[7] = '07:00';
$times[8] = '08:00';
$times[9] = '09:00';
$times[10] = '10:00';
$times[11] = '11:00';
$times[12] = '12:00';
$times[13] = '13:00';
$times[14] = '14:00';
$times[15] = '15:00';
$times[16] = '16:00';
$times[17] = '17:00';
$times[18] = '18:00';
$times[19] = '19:00';
$times[20] = '20:00';
$times[21] = '21:00';
$times[22] = '22:00';
$times[23] = '23:00';


    
///////////////////////////////////////////////////////////////////////////////
// Form
///////////////////////////////////////////////////////////////////////////////
echo form_open('bmbackup');
echo form_header(lang('bmbackup_schedule'), array('id' => 'schedule_form'));

echo field_dropdown('hour', $times, $hour, lang('bmbackup_time'), FALSE);
echo field_button_set($buttons);

echo form_footer();
echo form_close();
